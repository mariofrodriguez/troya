//###################LIBRERIAS#######################
import Vue from 'vue';
import Router from 'vue-router';
import VueResource from 'vue-resource';
import VueMaterial from 'vue-material';
import 'vue-material/dist/vue-material.min.css';
import VeeValidate from 'vee-validate';
import VueLocalStorage from 'vue-localstorage';
import VueCharts from 'vue-chartjs';
import { Bar, Line, Pie } from 'vue-chartjs';
import * as VueGoogleMaps from 'vue2-google-maps';
import VueAsyncData from 'vue-async-data';
import JsonExcel from 'vue-json-excel';
import moment from 'moment'

import BackToTop from 'vue-backtotop'
import VueUploadComponent from "vue-upload-component";
// In your VueJS component.
import { VueperSlides, VueperSlide } from 'vueperslides'
import VueBreadcrumbs from 'vue-breadcrumbs'
// Main JS (in UMD format)
import VueTimepicker from 'vue2-timepicker'
// CSS
import 'vue2-timepicker/dist/VueTimepicker.css'

import VuePhoneNumberInput from 'vue-phone-number-input';
import 'vue-phone-number-input/dist/vue-phone-number-input.css';

Vue.component("vueper-slide", VueperSlide);
Vue.component("vueper-slides", VueperSlides);
Vue.component("vue-timepicker", VueTimepicker);
Vue.component('vue-phone-number-input', VuePhoneNumberInput);

Vue.use(VueBreadcrumbs);
Vue.use(BackToTop);
// Vue.use(Firebase);
// Vue.use(VueFirestore);
Vue.use(VueMaterial);
Vue.use(Router);
Vue.use(VeeValidate, {
  fieldsBagName: 'vvFields'
});
Vue.use(VueResource);
Vue.use(VueLocalStorage);
Vue.use(VueCharts);
Vue.use(Bar);
Vue.use(Line);
Vue.use(VueAsyncData);
Vue.use(Pie);
Vue.use(VueGoogleMaps, {
  load: {
    key: 'AIzaSyBV_r5mWF8CnKWGmQAtnCgj0-zko1Mgjlc',
    v: '0.0.2',
    region: 'ES',
    libraries: 'places'
  }
});

Vue.component('file-upload', VueUploadComponent);
Vue.component('downloadExcel', JsonExcel);
Vue.component('line-chart', Line);
Vue.component('bar-chart', Bar);
Vue.component('pie-chart', Pie);

Vue.filter('formatDate', function(value) {
  if (value) {
    return moment(String(value)).format('DD/MM/YYYY HH:mm')
  }
})
Vue.filter('formatSize', function (size) {
  if (size > 1024 * 1024 * 1024 * 1024) {
    return (size / 1024 / 1024 / 1024 / 1024).toFixed(2) + ' TB'
  } else if (size > 1024 * 1024 * 1024) {
    return (size / 1024 / 1024 / 1024).toFixed(2) + ' GB'
  } else if (size > 1024 * 1024) {
    return (size / 1024 / 1024).toFixed(2) + ' MB'
  } else if (size > 1024) {
    return (size / 1024).toFixed(2) + ' KB'
  }
  return size.toString() + ' B'
})
import global from '../global'
Vue.mixin(global);

////###############################CONFIGURACIÓN WEB SOCKETS#################################
import ReconnectingWebSocket from 'reconnecting-websocket'
const options = {
  maxReconnectionDelay: 10000,
  minReconnectionDelay: 1000,
  reconnectionDelayGrowFactor: 1.3,
  connectionTimeout: 4000,
  maxRetries: 1,
  debug: false,
}
// export const socket_incidencias = new ReconnectingWebSocket("ws:134.209.42.171:8002/ws/chat/incidencias_arroba/", [], options);
// export const socket_incidencias = new ReconnectingWebSocket("ws:192.168.100.9:8000/ws/chat/incidencias_arroba/", [], options);
// export const socket_capacitacion = new ReconnectingWebSocket("ws:134.209.42.171:8002/ws/chat/capacitaciones_arroba/", [], options);
// export const socket_metas = new ReconnectingWebSocket("ws:134.209.42.171:8002/ws/chat/metas_arroba/", [], options);
// export const socket_notifications = new ReconnectingWebSocket("ws:134.209.42.171:8002/ws/chat/notifications/", [], options);

export const socket_incidencias = new ReconnectingWebSocket("wss:vm-icov-001.riosoft.cl/ws/chat/incidencias_arroba/", [], options);
export const socket_capacitacion = new ReconnectingWebSocket("wss:vm-icov-001.riosoft.cl/ws/chat/capacitaciones_arroba/", [], options);
export const socket_metas = new ReconnectingWebSocket("wss:vm-icov-001.riosoft.cl/ws/chat/metas_arroba/", [], options);
export const socket_notifications = new ReconnectingWebSocket("wss:vm-icov-001.riosoft.cl/ws/chat/notifications/", [], options);

var router = new Router({
  routes: [{
      path: '/',
      name: 'Login',
      component: () => import('@/components/general/login'),
    },
    {
      path: '/recuperar',
      name: 'Recover',
      component: () => import('@/components/general/recover'),
    },
    {
      path: '*',
      component: () => import('@/components/general/notFound'),
    },
    {
      path: '/menu',
      name: 'Menu',
      component: () => import('@/components/general/menu'),
      children: [
        {
          path: '/Home',
          name: 'Home',
          component: () => import('@/components/general/home'),
          props: true
        },
        {
          path: '/Schedule',
          name: 'Schedule',
          component: () => import('@/components/administration/schedule'),
          props: true
        },
        {
          path: '/report-segmentation-0-30',
          name: 'ReportSegmentation030',
          component: () => import('@/components/reports/reportSegmentation030'),
          props: true
        },        
        {
          path: '/report-segmentation-historic/',
          name: 'ReportSegmentationHist',
          component: () => import('@/components/reports/reportSegmentationHistorical'),
          props: true
        },
        {
          path: '/region-report/',
          name: 'ReportSegmentationRegion',
          component: () => import('@/components/reports/reportSegmentationRegion'),
          props: true
        },
        {
          path: '/commune-report/',
          name: 'ReportSegmentationCommune',
          component: () => import('@/components/reports/reportSegmentationCommune'),
          props: true
        },
        {
          path: '/market-research/',
          name: 'marketResearch',
          component: () => import('@/components/reports/marketResearch'),
          props: true
        },
        {
          path: '/segmentation-report/',
          name: 'ReportSegmentation',
          component: () => import('@/components/reports/reportSegmentation'),
          props: true
        },
        {
          path: '/salesMan',
          name: 'salesMan',
          component: () => import('@/components/reports/reportSalesMan'),
          props: true
        },
        {
          path: '/salesManPublish',
          name: 'salesManPublish',
          component: () => import('@/components/reports/reportSalesManPublish'),
          props: true
        },
        {
          path: '/salesManRoute',
          name: 'salesManRoute',
          component: () => import('@/components/reports/reportsSalesManRoute'),
          props: true
        },
        {
          path: '/salesManCommission',
          name: 'salesManCommission',
          component: () => import('@/components/reports/reportSalesManCommission'),
          props: true
        },
        {
          path: '/salesManCollection',
          name: 'salesManCollection',
          component: () => import('@/components/reports/reportSalesManCollection'),
          props: true
        },
        {
          path: '/reportSales',
          name: 'reportSales',
          component: () => import('@/components/reports/reportSubsidiarySales'),
          props: true
        },
        {
          path: '/reportActivation',
          name: 'reportActivation',
          component: () => import('@/components/reports/reportActivation'),
          props: true
        },
        {
          path: '/reportPublish',
          name: 'reportPublish',
          component: () => import('@/components/reports/reportPublish'),
          props: true
        },
        {
          path: '/reportRewards',
          name: 'reportRewards',
          component: () => import('@/components/reports/reportRewards'),
          props: true
        },
        {
          path: '/reportConcesion',
          name: 'reportConcesion',
          component: () => import('@/components/reports/reportConcesion'),
          props: true
        },
        {
          path: '/reportCapacitation',
          name: 'reportCapacitation',
          component: () => import('@/components/reports/reportCapacitation'),
          props: true
        },
        {
          path: '/reportStock',
          name: 'reportStock',
          component: () => import('@/components/reports/reportStock'),
          props: true
        },
        {
          path: '/reportSubsidiaryCommissionSales',
          name: 'reportSubsidiaryCommissionSales',
          component: () => import('@/components/reports/reportSubsidiaryCommissionSales'),
          props: true
        },
        {
          path: '/reportSubsidiaryCommissionEnable',
          name: 'reportSubsidiaryCommissionEnable',
          component: () => import('@/components/reports/reportSubsidiaryCommissionEnable'),
          props: true
        },
        {
          path: '/reportPorted',
          name: 'reportPorted',
          component: () => import('@/components/reports/reportPorted'),
          props: true
        },
        {
          path: '/reportSurvey',
          name: 'reportSurvey',
          component: () => import('@/components/reports/reportSurvey'),
          props: true
        },
        {
          path: '/reportSubsidiaryNoVisits',
          name: 'reportSubsidiaryNoVisits',
          component: () => import('@/components/reports/reportSubsidiaryNoVisits'),
          props: true
        },
        {
          path: '/reportSubsidiaryVisits',
          name: 'reportSubsidiaryVisits',
          component: () => import('@/components/reports/reportSubsidiaryVisits'),
          props: true
        },
        {
          path: '/reportSchedule',
          name: 'reportSchedule',
          component: () => import('@/components/reports/reportSchedule'),
          props: true
        },  
        {
          path: '/reportProspectChecked',
          name: 'reportProspectChecked',
          component: () => import('@/components/reports/reportProspectChecked'),
          props: true
        }, 
        {
          path: '/reportExecutive',
          name: 'reportExecutive',
          component: () => import('@/components/reports/reportExecutive'),
          props: true
        },         
        {
          path: '/activationCharts/:month,type,year',
          name: 'activationCharts',
          component: () => import('@/components/reports/activationCharts'),
          props: true
        },
        {
          path: '/configuracion-usuario',
          name: 'ConfigUser',
          component: () => import('@/components/administration/configUser'),
          props: true
        },
        {
          path: '/configuracion-group',
          name: 'ConfigGroup',
          component: () => import('@/components/administration/configGroup'),
          props: true
        },
        {
          path: '/configuracion-sucursal',
          name: 'ConfigSubsidiary',
          component: () => import('@/components/administration/configSubsidiary'),
          props: true
        },
        {
          path: '/configuracion-categoria',
          name: 'ConfigCategory',
          component: () => import('@/components/administration/configCategory'),
          props: true
        },
        {
          path: '/configuracion-preguntas',
          name: 'ConfigQuestion',
          component: () => import('@/components/administration/configQuestion'),
          props: true
        },
        {
          path: '/value-authorizations',
          name: 'valueAuthorizations',
          component: () => import('@/components/administration/valueAuthorizations'),
          props: true
        },
        {
          path: '/subsidiaryMaps',
          name: 'subsidiaryMaps',
          component: () => import('@/components/maps/subsidiaryMaps'),
          props: true
        },
        {
          path: '/capacitationMap',
          name: 'CapacitationMap',
          component: () => import('@/components/maps/capacitationMap'),
          props: true
        },
        {
          path: '/publishMap',
          name: 'PublishMap',
          component: () => import('@/components/maps/publishMap'),
          props: true
        },
        {
          path: '/scoredSubsidiaryMaps/:type',
          name: 'scoredSubsidiaryMaps',
          component: () => import('@/components/maps/scoredSubsidiaryMaps'),
          props: true
        },
        {
          path: '/scoredDiarySubsidiaryMaps',
          name: 'scoredDiarySubsidiaryMaps',
          component: () => import('@/components/maps/scoredDiarySubsidiaryMaps'),
          props: true
        },
        {
          path: '/importaciones/:id,name',
          name: 'ImportsParam',
          component: () => import('@/components/general/imports'),
          props: true
        },
        {
          path: '/importaciones',
          name: 'Imports',
          component: () => import('@/components/general/imports'),
          props: true
        },
        {
          path: '/catalogo-productos',
          name: 'ProductCatalog',
          component: () => import('@/components/reports/productCatalog'),
          props: true
        },
        {
          path: '/tipo',
          name: 'ConfigType',
          component: () => import('@/components/administration/configType'),
          props: true
        },
        {
          path: '/encargado-comuna',
          name: 'ConfigManagerCommune',
          component: () => import('@/components/administration/configManagerCommune'),
          props: true
        },
        {
          path: '/producto',
          name: 'ConfigProduct',
          component: () => import('@/components/administration/configProduct'),
          props: true
        },
        {
          path: '/punto',
          name: 'ConfigPoint',
          component: () => import('@/components/administration/configPoint'),
          props: true
        },
        {
          path: '/estados-agenda',
          name: 'ConfigState',
          component: () => import('@/components/administration/configState'),
          props: true
        },
        {
          path: '/estados-sucursal',
          name: 'ConfigSubsidiaryState',
          component: () => import('@/components/administration/configSubsidiaryState'),
          props: true
        },
        {
          path: '/chip-validator',
          name: 'chipValidator',
          component: () => import('@/components/administration/chipValidator'),
          props: true
        },
        {
          path: '/capacitacion',
          name: 'capacitacion',
          component: () => import('@/components/messaging/capacitacion'),
          props: true
        },
        {
          path: '/metas',
          name: 'metas',
          component: () => import('@/components/messaging/metas'),
          props: true
        },
        {
          path: '/incidencias',
          name: 'incidencias',
          component: () => import('@/components/messaging/incidencias'),
          props: true
        },
        {
          path: '/faq',
          name: 'configFaq',
          component: () => import('@/components/administration/configFaq'),
          props: true
        }
      ]
    }
  ]
})
// authentication router
router.beforeEach((to, from, next) => {
  if(to.matched.some(record => record.meta.requiresAuth)) {
    if (localStorage.getItem('working') == null) {
    next({ path: '/', params: { nextUrl: to.fullPath }})
    } else next()
  } else next()
})

export default router

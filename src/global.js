import Vue from 'vue';
import LocalStorage from 'vue-localstorage'
Vue.component(LocalStorage);

export default {
  data: function () {
    return { 
      count_timer: 0
    }
  },
  methods: {
    //######################################################
    // asigna url base
    setUrlBase(val){
      this.$localStorage.set("urlBase", val);
    },
    // retorna url base
    getUrlBase(){
      return this.$localStorage.get("urlBase");
    },
    // retorna el access_token
    getToken(){
      return this.$localStorage.get("access_token");
    },
    // retorna el refresh_token
    getRefreshToken(){
      return this.$localStorage.get("refresh_token");
    },
    //retorna el HTTP HEADER
    getHeaders(){
      return JSON.parse(this.$localStorage.get("request_headers"));
    },
    getHHeaders(){
      return JSON.parse(this.$localStorage.get("headers"));
    },
    // asigna el tipo de autenticación + access_token a HTTP HEADER
    updateHeaders(auth){
      this.$localStorage.remove("request_headers");
      this.$localStorage.set("headers",JSON.stringify({ Authorization: auth }));
      this.$localStorage.set("request_headers",JSON.stringify({ headers: { Authorization: auth }}));
    },
    setClientID(val){
      this.$localStorage.set("client_id", val)
    },
    setClientSecret(val){
      this.$localStorage.set("client_secret", val)
    },
    getClientID(){
      return this.$localStorage.get("client_id")
    },
    getClientSecret(){
      return this.$localStorage.get("client_secret")
    },
    // actualiza los valores de variables globales de access_token + refresh_token
    updateToken(access, refresh, expires_in){
      this.$localStorage.set("working", true)
      this.$localStorage.set("access_token", access)
      this.$localStorage.set("refresh_token", refresh)
      this.$localStorage.set("request_headers",JSON.stringify({ headers: { Authorization: "Bearer " + access }}))
      this.$localStorage.set("headers",JSON.stringify({ Authorization: "Bearer " + access }))
      this.$localStorage.set('expire_time_token',expires_in)
      this.evalua_time_token()
    },
    evalua_time_token() {
      if(this.$localStorage.get('expire_time_token') != null)
      this.token_timeout = setTimeout(() => {
        this.refreshToken()
      }, this.$localStorage.get('expire_time_token') * 1000)
    },
    // actualiza el token de acceso y el header
    async refreshToken(){
      var form_data = new FormData()
      form_data.append("grant_type", "refresh_token")
      form_data.append("client_id", this.getClientID())
      form_data.append("client_secret", this.getClientSecret())
      form_data.append("refresh_token", this.getRefreshToken())
      
      const response = await this.$http.post(this.getUrlBase() +"/o/token/", form_data, this.getHeaders())
      let auth = response.body
      this.$localStorage.set("access_token", auth.access_token)
      this.$localStorage.set("refresh_token", auth.refresh_token)
      this.$localStorage.set('expire_time_token',JSON.stringify(auth.expires_in))
      this.$localStorage.set("request_headers",JSON.stringify({ headers: { Authorization: "Bearer " + auth.access_token }}))
      clearTimeout(this.token_timeout)
      this.evalua_time_token()
    }
  }
}